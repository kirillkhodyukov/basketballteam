﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lesson52.interfaces
{
    public interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
