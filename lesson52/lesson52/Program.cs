﻿using lesson52.models;
using System;
using System.Linq;

namespace lesson52
{
    class Program
    {
        static void Main(string[] args)
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                if (!db.Teams.Any())
                {
                    var team = new Team
                    {
                        Name = "Los-Angeles Lakers",
                        City = "Los-Angeles",
                        Conference = Conference.Western,
                        DateOfCreate = new DateTime(1947),
                        SeasonWins = 100,
                        SeasonLose = 0
                    };

                    var coach = new Coach
                    {
                        Name = "Frank",
                        Surname = "Vogel",
                        DateOfBirth = new DateTime(1973,6,21),
                        Team = team,
                        YearOfCoaching = 20
                    };

                    var player1 = new Player
                    {
                        Name = "LeBron",
                        Surname = "James",
                        DateOfBirth = new DateTime(1984, 12, 30),
                        Team = team,
                        TeamID = team.ID,
                        Growth = 2.06,
                        Weight = 110,
                        JerseyNumber = 23,
                        Position = Position.SF
                    };

                    var player2 = new Player
                    {
                        Name = "Russell",
                        Surname = "Westbrook",
                        DateOfBirth = new DateTime(1988, 11, 18),
                        Team = team,
                        TeamID = team.ID,
                        Growth = 1.91,
                        Weight = 101,
                        JerseyNumber = 0,
                        Position = Position.PG
                    };

                    var player3 = new Player
                    {
                        Name = "Anthony",
                        Surname = "Davis",
                        DateOfBirth = new DateTime(1993, 3, 11),
                        Team = team,
                        TeamID = team.ID,
                        Growth = 2.27,
                        Weight = 115,
                        JerseyNumber = 3,
                        Position = Position.PF
                    };

                    var player4 = new Player
                    {
                        Name = "Austin",
                        Surname = "Reaves",
                        DateOfBirth = new DateTime(1998, 5, 29),
                        Team = team,
                        TeamID = team.ID,
                        Growth = 1.96,
                        Weight = 100,
                        JerseyNumber = 12,
                        Position = Position.SG
                    };

                    var player5 = new Player
                    {
                        Name = "Dwight",
                        Surname = "Howard",
                        DateOfBirth = new DateTime(1985,12,8),
                        Team = team,
                        TeamID = team.ID,
                        Growth = 2.08,
                        Weight = 120,
                        JerseyNumber = 39,
                        Position = Position.C
                    };

                    db.Teams.Add(team);
                    db.SaveChanges();

                    db.Persons.Add(coach);
                    db.Persons.Add(player1);
                    db.Persons.Add(player2);
                    db.Persons.Add(player3);
                    db.Persons.Add(player4);
                    db.Persons.Add(player5);
                    db.SaveChanges();
                }
            }
        }
    }
}
