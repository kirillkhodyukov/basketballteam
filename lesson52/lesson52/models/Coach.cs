﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace lesson52.models
{
    public class Coach : Person
    {
        [ForeignKey("Team")]
        public int TeamID { get; set; }
        public int YearOfCoaching { get; set; }
        public Team Team { get; set; }
    }
}
