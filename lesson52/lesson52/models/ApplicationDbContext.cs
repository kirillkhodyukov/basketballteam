﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace lesson52.models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Coach> Coach { get; set; }
        public DbSet<Player> Players { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\User\Desktop\с# 4 модуль\basketballteam\lesson52\lesson52");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder.Entity<Player>()
                .HasIndex(x => new { x.TeamID, x.JerseyNumber })
                .IsUnique();

            modelBuilder.Entity<Team>()
                .HasOne(x => x.Coach)
                .WithOne(x => x.Team)
                .HasForeignKey<Coach>(x => x.TeamID)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Team>()
                .HasMany(x => x.Plyers)
                .WithOne(x => x.Team)
                .HasForeignKey(x => x.TeamID)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}