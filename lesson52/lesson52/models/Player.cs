﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace lesson52.models
{
    public class Player : Person
    {
        [ForeignKey("Team")]
        public int TeamID { get; set; }

        public double Growth { get; set; }
        public double Weight { get; set; }
        public int JerseyNumber { get; set; }
        public Position Position { get; set; }

        public Team Team { get; set; }
    }
}
