﻿using lesson52.interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace lesson52.models
{
    public class Team : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Coach")]
        public int CoachID { get; set; }

        [Required]
        public string Name { get; set; }

        public string City { get; set; }
        public Conference Conference { get; set; }

        [Required]
        public DateTime DateOfCreate { get; set; }
        public int SeasonWins { get; set; }
        public int SeasonLose { get; set; }
        public Coach Coach { get; set; }

        public List<Player> Plyers { get; set; }
    }
}
